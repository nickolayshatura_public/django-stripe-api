from django.urls import path
from .views import item_view, buy_item, cancel

urlpatterns = [
    path('item/<int:id>/', item_view, name='item'),
    path('buy/<int:id>/', buy_item, name='buy'),
    path('cancel/', cancel, name='cancel')
]
