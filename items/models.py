from django.db import models
from django.contrib import admin


class Item(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    price = models.FloatField()

    def __str__(self):
        return self.name


admin.site.register(Item)