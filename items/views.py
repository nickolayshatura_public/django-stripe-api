import stripe
from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Item
from testWork.settings import STRIPE_SECRET_KEY as secret_key, STRIPE_PUBLISHABLE_KEY as public_key

stripe.api_key = secret_key

def item_view(request, id):
    item = Item.objects.get(id=id)
    context = {'key': public_key, 'item': item}
    return render(request, 'item.html', context)


@api_view(['GET'])
def buy_item(request, id):
    item = Item.objects.get(id=id)
    session = stripe.checkout.Session.create(
        payment_method_types=['card'],
        line_items=[
            {
                'price_data': {
                    'currency': 'usd',
                    'product_data': {
                        'name': item.name,
                        'description': item.description
                    },
                    'unit_amount': int(item.price*100),
                },
                'quantity': 1,
            },
        ],
        success_url='http://localhost:8000/success',
        cancel_url='http://localhost:8000/cancel',
        mode='payment'
    )
    return Response({'id': session.id})


def cancel(request):
    return render(request, 'cancel.html')
