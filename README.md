# Django-StripeAPI Project

Пример проекта Django, который реализует Stripe API для обработки платежей.

## Getting Started

Эти инструкции помогут вам запустить и запустить копию проекта на вашем локальном компьютере для целей разработки и тестирования.


### Prerequisites
1. [Docker](https://www.docker.com/)
2. [Docker Compose](https://www.docker.com/compose/)

### Installing
1. Клонировать репозиторий
`$ git clone https://gitlab.com/nickolayshatura_public/django-stripe-api.git`
2. Перейдите в каталог проекта
`$ cd django-stripe-api`
3. Создайте контейнеры Docker
`$ docker-compose build`
4. Создайте суперпользователя для административного интерфейса Django.
`$ docker-compose run web python manage.py createsuperuser`
5. Запустите сервер
`$ docker-compose up`

Приложение будет доступно по адресу http://localhost:8000.

### Deployment
Приложение можно развернуть в производственной среде с помощью Docker Compose.
### Built With
1. [Django](https://www.djangoproject.com/) - The web framework used 
2. [Stripe](https://stripe.com/docs/api) - Payment processing API 
3. [Docker](https://www.docker.com/) - Containerization technology

### Author
Shatura Nickolay, 2023.

### License
Этот проект находится под лицензией MIT License — подробности см. в файле LICENSE.md.