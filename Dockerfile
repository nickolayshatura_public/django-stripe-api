# Use an official Python runtime as the base image
FROM python:3.10

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install dependencies
RUN pip install --no-cache-dir -r requirements.txt
RUN python manage.py makemigrations
RUN python manage.py migrate
RUN python manage.py makemigrations items
RUN python manage.py migrate

# Run the command to start the Django server
CMD python manage.py runserver 0.0.0.0:8000